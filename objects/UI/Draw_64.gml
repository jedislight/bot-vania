/// @description Insert description here
// You can write your code in this editor
var bot = instance_find(Bot, 0);
if (instance_exists(bot)){
	power_offset = bot.power_current;
	thermal_offset = bot.thermal_current;
	hydro_offset = bot.hydro_charge
}

power_offset_max = global.power_tolerance_max;
thermal_offset_max = global.thermal_tolerance_max;
power_offset_min = global.power_tolerance_min
thermal_offset_min = global.thermal_tolerance_min;
hydro_offset_max = global.hydro_charge_max

center = 1366  / 2
draw_set_halign(fa_center)
draw_set_color(c_white);
draw_text(center+power_offset_min, 64, "[")
draw_text(center, 64, "|")
draw_text(center+power_offset_max, 64, "]")
if (alert_toggle and power_offset < power_offset_min + (0.15 * (power_offset_max - power_offset_min))){
	draw_text(center+power_offset_min - 16, 64, "!!!");
}
if (alert_toggle and power_offset > power_offset_max){
	draw_text(center+power_offset_max + 16, 64, "!!!");
}

draw_text(center+thermal_offset_min, 128, "[")
draw_text(center, 128, "|")
draw_text(center+thermal_offset_max, 128, "]")
if (alert_toggle and thermal_offset < thermal_offset_min){
	draw_text(center-thermal_offset_max - 16, 128, "!!!");
}

if (global.upgrades[? "HYDRO"]) {
draw_text(center-hydro_offset_max div 2, 192, "[")
draw_text(center, 192, "|")
draw_text(center+hydro_offset_max div 2, 192, "]")	
}
draw_set_halign(fa_left)
x1 = center
x2 = center + thermal_offset
y1 = 96
y2 = 128
lcolor = c_white
rcolor = c_red
if(thermal_offset < 0) {
	x2 = center
	x1 = center + thermal_offset
	lcolor = c_blue
	rcolor = c_white
}
draw_rectangle_color(x1, y1, x2, y2, lcolor, rcolor, rcolor, lcolor, false);

x1 = center + power_offset_min
x2 = min(center, center + power_offset)
y1 = 32
y2 = 64
lcolor = c_dkgray
rcolor = c_yellow
draw_rectangle_color(x1, y1, x2, y2, lcolor, rcolor, rcolor, lcolor, false);
x1 = center
x2 = max(center, center+power_offset)
lcolor = c_yellow
rcolor = c_orange
draw_rectangle_color(x1, y1, x2, y2, lcolor, rcolor, rcolor, lcolor, false);
if (global.upgrades[? "HYDRO"]) {
	x1 = center - hydro_offset_max div 2
	x2 = x1 + hydro_offset
	y1 = 160
	y2 = 192
	lcolor = c_blue
	rcolor = c_aqua	
	draw_rectangle_color(x1, y1, x2, y2, lcolor, rcolor, rcolor, lcolor, false);
}
