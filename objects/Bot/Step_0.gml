event_inherited()
// Get input
var input_left = -keyboard_check(vk_left);
var input_right = keyboard_check(vk_right);
var input_up = keyboard_check_pressed(vk_up);
var input_down = keyboard_check_pressed(vk_up);

var input_ground_gear = keyboard_check(ord("Z"))
var input_core_gear = keyboard_check(ord("X"))
var input_top_gear = keyboard_check(ord("C"))

var input_ground_gear_pressed = keyboard_check_pressed(ord("Z"))
var input_core_gear_pressed = keyboard_check_pressed(ord("X"))
var input_top_gear_pressed = keyboard_check_pressed(ord("C"))

// Override input if overcharged
if (power_current > global.power_tolerance_max) {
	input_left = -choose(0, 0, 1);
	input_right = choose(0, 0, 1)
	input_up = choose(0, 0, 1)
	input_down = choose(0, 0, 1)

	input_ground_gear = choose(0, 0, 1)
	input_core_gear = choose(0, 0, 1)
	input_top_gear = choose(0, 0, 1)

	input_ground_gear_pressed = choose(0, 0, 1)
	input_core_gear_pressed = choose(0, 0, 1)
	input_top_gear_pressed = choose(0, 0, 1)
	
	if (10 > random(100))
		instance_create_layer(x,y,"Zones", OverchargeEffectBolt);
}

// Use input
var power_consumption = 0;
if (on_ground) 
{
	if (upgrades[? "TREADS"]) {
		var move = input_left + input_right;
		power_consumption += abs(move) *.001;
	}
	
	hsp = move * moveSpeed;
}

if (on_ground) 
	if (upgrades[? "SPRING"]){
		vsp = input_ground_gear_pressed * -jumpSpeed
		power_consumption += input_ground_gear_pressed;
	}

if (upgrades[? "HYDRO"]) 
{
	if (hydro_charge > 0) 
	{
		vsp -= grav * input_core_gear;
		hydro_charge -= input_core_gear;
		if(input_core_gear and hydro_charge mod 5 == 0)
		{
			var e = instance_create_layer(x, y+16, "BackgroundInstances", HydroEjection);
			e.vsp = vsp + grav * 120;
			e.hsp = hsp;
			e.terminal_vsp *= 80;
		}
	}
	hydro_charge += on_ground - input_core_gear;
	hydro_charge = clamp(hydro_charge, 0, global.hydro_charge_max);
}

// Check for specific collisions
if ((place_meeting(x,y+1, Hazard) and on_ground) or instance_exists(collision_point(x,y,Hazard,false,false))) {
	var C = 75;
	if (thermal_current < C)
		thermal_current += 1
}

var cold_zone = collision_point(x,y,ColdZone,false,false)
if (instance_exists(cold_zone)) {
	if (cold_zone.C < thermal_current){
		thermal_current -= .2;	
	}
}


if (instance_exists(collision_point(x,y,RechargeZone,false,false))) {
		power_current += .1;	
}

// apply power/thermal effects
power_current -= power_consumption
if (power_current > global.power_tolerance_max)
{
	power_current -= power_consumption * 10;
	thermal_current += power_consumption
}
thermal_current -= sign(thermal_current) * .1;

if (thermal_current < global.thermal_tolerance_min) {
	var under = abs(global.thermal_tolerance_min - thermal_current);
	power_current -= under * .01;
}

var dead = thermal_current > global.thermal_tolerance_max or power_current < global.power_tolerance_min or power_current > global.power_tolerance_max*2;
if (dead) instance_destroy(id);
