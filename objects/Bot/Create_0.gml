/// @description Insert description here
// You can write your code in this editor

event_inherited();
jumpSpeed = 8;
moveSpeed = 2;
if( not variable_global_exists("upgrades")) 
{
	global.upgrades = ds_map_create();
	upgrades = global.upgrades;
	upgrades[? "TREADS"] = true;
	upgrades[? "SPRING"] = false;
	upgrades[? "HYDRO"] = false;
	

	global.hydro_charge_max = room_speed;
	global.thermal_tolerance_max = 25
	global.thermal_tolerance_min = -25
	global.power_tolerance_max = 50
	global.power_tolerance_min = -50
}
else 
{
	upgrades = global.upgrades;
}


hydro_charge = 0
thermal_current = 0
power_current = 0