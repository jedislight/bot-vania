/// @description Insert description here
// You can write your code in this editor

hsp += frame_hsp;
vsp += frame_vsp;
var zero_hsp = false;
var zero_vsp = false;
// H Collisions
if (place_meeting(x + hsp, y, Ground))
{
	while (!place_meeting(x + sign(hsp), y, Ground))
	{
		x += sign(hsp);
	}
	zero_hsp = true;
}
else x += hsp;

// V Collisions
if (place_meeting(x, y + vsp, Ground))
{
	while (!place_meeting(x, y + sign(vsp), Ground))
	{
		y += sign(vsp); 
	}
	zero_vsp = true;
	vsp = 0;
} else y += vsp;

if (zero_hsp)
	hsp = 0;
else
	hsp -= frame_hsp;

if (zero_vsp)
	vsp = 0
else
	vsp -= frame_vsp;
	
frame_hsp = 0;
frame_vsp = 0;