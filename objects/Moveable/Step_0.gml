/// @description Insert description here
// You can write your code in this editor

// Get state
on_ground = place_meeting(x, y + 1, Ground);

if (not on_ground)
{
	if (vsp < terminal_vsp) 
	{
		vsp += grav; 
		vsp = min(vsp, terminal_vsp);
	}
}